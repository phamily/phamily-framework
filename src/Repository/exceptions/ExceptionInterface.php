<?php

namespace Phamily\Framework\Repository\exceptions;

use Phamily\Framework\PhamilyExceptionInterface;

interface ExceptionInterface extends PhamilyExceptionInterface
{
}
