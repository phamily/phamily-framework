<?php

namespace Phamily\Framework\Repository\exceptions;

class NotFoundException extends \OutOfBoundsException implements ExceptionInterface
{
}
