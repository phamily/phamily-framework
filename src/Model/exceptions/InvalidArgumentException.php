<?php

namespace Phamily\Framework\Model\exceptions;

class InvalidArgumentException extends \InvalidArgumentException implements ExceptionInterface
{
}
