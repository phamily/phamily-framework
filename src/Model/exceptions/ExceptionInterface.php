<?php

namespace Phamily\Framework\Model\exceptions;

use Phamily\Framework\PhamilyExceptionInterface;

/**
 * namespaced stuff for type hinting
 * inheritance in every layer.
 * 
 * @author samizdam
 */
interface ExceptionInterface extends PhamilyExceptionInterface
{
}
