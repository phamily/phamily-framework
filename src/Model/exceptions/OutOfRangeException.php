<?php

namespace Phamily\Framework\Model\exceptions;

class OutOfRangeException extends \OutOfRangeException implements ExceptionInterface
{
}
