<?php

namespace Phamily\Framework\Model\exceptions;

class OutOfBoundsException extends \OutOfBoundsException implements ExceptionInterface
{
}
