<?php

namespace Phamily\Framework\Model\exceptions;

class LogicException extends \LogicException implements ExceptionInterface
{
}
